Aleeious Engine
===========

A Battlemail Kung Fu app engine for the mobile web.

Requirements
============
PHP 5.4+  
MySQL 5.1

Installing
==========
1) Copy Source files into drectory of your choice  
2) Edit config.php with your database details and modify setting as you see fit
3) Go to yoursite/install.php and follow the instructions to install the database tables
4)Create the administrator account by registering at yoursite/register.php  
5 Enjoy

Modifying the layout
====================
The output of the pages can be modified by editing the .tpl files inside the templates directory. The names of the template
file is the same as the page it is displayed on except messagedialog.tpl which is for displaying errors. All script related
output is placed inside <?php echo $bla ?> tags in the template file itself.

Removing The Footer
====================
You may remove the footer from all the pages if you wish, but please don't claim to own the script/engine. Keeping the footer
intact is appreciated as it promotes the project. To remove the footer simply remove the following code from all the template
files(.tpl) inside the template directory. The code can be found at the very bottom of each template file:
```
<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="2">Powered By The Aleeious Engine</a></p></div>
```