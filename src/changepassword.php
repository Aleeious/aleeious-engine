<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} // otherwise if the form hasn't been submitted
else {
    if (!isset($_POST["submit"])) {
        // update the session timeout timer
        $_SESSION['lastactivity'] = time();

        // display it
        $chops->display('changepassword.tpl');

        // and terminate
        exit;
    } // otherwise the form has been submitted
    else {
        // update the session timeout timer
        $_SESSION['lastactivity'] = time();

        // if the password field is empty
        if (empty($_POST["password"])) {
            // set the dialog title
            $chops->title = 'Error';

            // sthe dialog message stating the defender field is empty
            $chops->message = 'current password is empty';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // if the password is too short or too big
        elseif (strlen($_POST["password"]) < 4 || strlen($_POST["password"]) > 16) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the password is too short or too big
            $chops->message = 'current password must be 4-16 characters long';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // otherwise the password is ok
        else {
            // set the dialog message title
            $_POST["password"] = trim(strip_tags($_POST["password"]));
        }

        // if the newpassword field is empty
        if (empty($_POST["newpassword"])) {
            // set the dialog title
            $chops->title = 'Error';

            // sthe dialog message stating the new password field is empty
            $chops->message = 'new password is empty';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // if the new password is too short or too big
        elseif (strlen($_POST["newpassword"]) < 6 || strlen($_POST["newpassword"]) > 32) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the new password is too short or too big
            $chops->message = 'new password must be 6-32 characters long';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // otherwise the new password is ok
        else {
            // set the new password
            $_POST["newpassword"] = trim(strip_tags($_POST["newpassword"]));
        }

        // if the verify password field is empty
        if (empty($_POST["verifypassword"])) {
            // set the dialog title
            $chops->title = 'Error';

            // sthe dialog message stating the verify password field is empty
            $chops->message = 'verify password is empty';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } elseif ($_POST["newpassword"] != $_POST["verifypassword"]) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the password is too short or too big
            $chops->message = 'password and verification password must match';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;

        } // otherwise the password is ok
        else {
            // set the verify password variable
            $_POST["verifypassword"] = trim(strip_tags($_POST["verifypassword"]));
        }

        // create instance of database class
        $database = new Database();

        // create instance of user class
        $user = new User($database);

        // if the current password is wrong
        if (!$user->checkLogin($_SESSION["username"], $_POST["password"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the current password is incorrect
            $chops->message = 'the current password is incorrect';

            // set the back url
            $chops->backurl = 'changepassword.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }

        if ($user->changePassword($_SESSION["username"], $_POST["newpassword"])) {
            // set the dialog title
            $chops->title = 'Info';

            // set the dialog message stating the password was changed successfully
            $chops->message = 'Your password has been changed';

            // set the back url
            $chops->backurl = 'profile.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } else {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the password change failed
            $chops->message = 'Your password could not be changed at this time';

            // set the back url
            $chops->backurl = 'profile.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }
    }
}

?>