<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// create an instance of the database class
$chops = new Chops();

// if the initial check was successful and the continue button was pressed
if (!isset($_POST["submit"])) {
    // set failed to false, checks will change it to false if necessary
    $failed = false;

    // set php version
    $chops->phpversion = phpversion();

    // if the php version isn't high enough
    if (version_compare(phpversion(), "5.3", "<")) {
        // set php version status to fail
        $chops->phpversionstatus = "Failed";

        // set failed to true
        $failed = true;
    } // otherwise it's sufficient
    else {
        // set php version status to success
        $chops->phpversionstatus = "Success";
    }

    // if the mysql extension is installed
    if (!extension_loaded("mysqli") && !function_exists('mysqli_connect')) {
        // get the mysqli version
        $chops->mysqliversion = "Not Installed";

        // set mysqli version status to failed
        $chops->mysqliversionstatus = "Failed";

        // set failed to true
        $failed = true;
    } // otherwise it's installed
    else {
        // get the mysqli version
        $chops->mysqliversion = phpversion("mysqli");

        // set mysqli version status to success
        $chops->mysqliversionstatus = "Success";
    }

    // if a test failed
    if ($failed) {
        // disable the continue button
        $chops->disabled = "disabled";
    } // otherwise the tests were successful
    else {
        // disable the continue button
        $chops->disabled = "";
    }

    // and display it
    $chops->display('install_step1.tpl');

    // and terminate
    exit;
} else {
    // create an instance of the database class
    $database = new Database();

    // create the match table
    if (!$database->createMatchTable()) {
        // set the table status to failed
        $chops->matchtablestatus = 'Fail';
    } else {
        // set the table status to succeeded
        $chops->matchtablestatus = 'Success';
    }

    // create the match index table
    if (!$database->createMatchIndexTable()) {
        // set the table status to failed
        $chops->matchindextablestatus = 'Failed';
    } else {
        // set the table status to succeeded
        $chops->matchindextablestatus = 'Success';
    }

    // create the user table
    if (!$database->createUserTable()) {
        // set the table status to failed
        $chops->usertablestatus = 'Failed';
    } else {
        // set the table status to succeeded
        $chops->usertablestatus = 'Success';
    }


    // and display it
    $chops->display('install_step2.tpl');

    // and terminate
    exit;
}

?>