<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration info
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user is logged in
if (isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the game
    header("Location: main.php");

    // and terminate
    exit;
} // if the form wasn't submitted
elseif (!isset($_POST["submit"])) {
    // display it
    $chops->display('forgotpassword.tpl');
} // otherwise the form was submitted
else {
    // if the username is empty
    if (empty($_POST["username"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the username is empty
        $chops->message = 'username is empty';

        // set the back url
        $chops->backurl = 'forgotpassword.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the username is too short or too big
    elseif (strlen($_POST["username"]) < 4 || strlen($_POST["username"]) > 16) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the username is empty
        $chops->message = 'username must be 4-16 characters long';

        // set the back url
        $chops->backurl = 'forgotpassword.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } elseif (!preg_match("#^[a-z0-9]*$#", $_POST["username"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the username contains invalid characters
        $chops->message = 'username contains invalid characters';

        // set the back url
        $chops->backurl = 'forgotpassword.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // otherwise the username is filled in
    else {
        // so sanitize it
        $_POST["username"] = trim(strip_tags($_POST["username"]));
    }

    // if the email is empty
    if (empty($_POST["email"])) {
        // set the message dialog
        $chops->title = 'Error';

        // set the dialog message stating the password is empty
        $chops->message = 'email is empty';

        // set the back url
        $chops->backurl = 'forgotpassword.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the email is invalid
    elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $_POST["email"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the email is invalid
        $chops->message = 'the email address you entered is invalid';

        // set the back url
        $chops->backurl = 'forgotpassword.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // otherwise the email is ok
    else {
        // so sanitize it
        $_POST["email"] = trim(strip_tags($_POST["email"]));
    }

    // create instance of database class
    $database = new Database();

    // create instance of user class
    $user = new User($database);

    // check the users login info
    if ($user->generateChangeRequest($_POST["username"], $_POST["email"])) {
        // if valid info was entered
        if ($user->getValidationKey()) {

            // generate the url dynamically
            $scripturl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

            // get the path
            $path = parse_url($scripturl, PHP_URL_PATH);

            // get just the directory the script resides
            $directory = dirname($path);

            if (!$user->email("Your Change Request",
                "Someone possibly you has asked to have your password changed. Please click on the link to continue: http://" . $_SERVER['HTTP_HOST'] . $directory . "resetpassword.php?username=" . $_POST["username"] . "&key=" . $user->getValidationKey() . " If you did not make this request, please contact the admin.")
            ) {

                // set the dialog title
                $chops->title = 'Error';

                // otherwise set the dialog message stating the password will be sent
                $chops->message = 'Could not send password reset email. Contact an admin';

                // set the back url
                $chops->backurl = 'index.php';

                // display it
                $chops->display('messagedialog.tpl');

                // and terminate
                exit;
            }
        }

        // set the dialog title
        $chops->title = 'Info';

        // otherwise set the dialog message stating the password will be sent
        $chops->message = 'If you have provided correct info, instructions for changing your password have been sent to the email provided.';

        // set the back url
        $chops->backurl = 'index.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } else {
        // set the dialog title
        $chops->title = 'Error';

        // otherwise display an error stating the database couldn't be contacted
        $chops->message = 'there was a problem contacting the database, please notify an admin';

        // set the back url
        $chops->backurl = 'index.php';

        // display it
        $chops->display('messagedialog.tpl');
    }
}

?>