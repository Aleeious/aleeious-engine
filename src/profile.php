<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
}

// update the session timeout timer
$_SESSION['lastactivity'] = time();

// create instance of database class
$database = new Database();

// create instance of user class
$user = new User($database);

// create instance of chops library
$chops = new Chops();

// retrieve the users info
$user->getInfo($_SESSION['username']);

// set the user's name
$chops->username = $_SESSION['username'];

// set the user's email
$chops->email = $user->getEmail();

// if the user didn't 'opted into notifications
if (!$user->getNotificationPreference()) {
    // set the user's notification prefernce
    $chops->notificationpreference = 'No';
} // otherwise the user opted in to notifications
else {
    // set the user's notification prefernce
    $chops->notificationpreference = 'Yes';
}

// set the user's xp
$chops->xp = $user->getXP();

// get the user's xp
$xp = $user->getXP();

// if the user falls under the first rank
if ($xp < RANK2_XP) {
    // set the user's rank as the first
    $chops->rank = RANK1_NAME;
} elseif ($xp >= RANK2_XP && $xp < RANK3_XP) {
    // set the user's rank as the second one
    $chops->rank = RANK2_NAME;
} elseif ($xp >= RANK3_XP && $xp < RANK4_XP) {
    // set the user's rank as the third one
    $chops->rank = RANK3_NAME;
} elseif ($xp >= RANK4_XP && $xp < RANK5_XP) {
    // set the user's rank as the fourth one
    $chops->rank = RANK4_NAME;
} elseif ($xp >= RANK5_XP && $xp < RANK6_XP) {
    // set the user's rank as the fifth one
    $chops->rank = RANK5_NAME;
} elseif ($xp >= RANK6_XP && $xp < RANK7_XP) {
    // set the user's rank as the sixth one
    $chops->rank = RANK6_NAME;
} elseif ($xp >= RANK7_XP && $xp < RANK8_XP) {
    // set the user's rank as the seventh one
    $chops->rank = RANK7_NAME;
} elseif ($xp >= RANK8_XP && $xp < RANK9_XP) {
    // set the user's rank as the eigth one
    $chops->rank = RANK8_NAME;
} else {
    // set the user's rank as the highest one
    $chops->rank = RANK9_NAME;
}

// display it
$chops->display('profile.tpl');

// and terminate
exit;

?>