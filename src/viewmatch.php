<?php

/*
* This file is part of Aleeious.
*
* Aleeious is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Aleeious is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Aleeious. If not, see <http://www.gnu.org/licenses/>.
*/

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {

// redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} // if the user didn't enter a match id
elseif (!isset($_GET["id"])) {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    // display it
    $chops->display('viewmatch.tpl');
} //otherwise the user is logged in, active and provided a match id
else {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    // if id isn't numeric
    if (!is_numeric($_GET["id"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set dialog message stating the id is invalid
        $chops->message = 'The id you entered is invalid';

        // set the back url
        $chops->backurl = 'viewmatch.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    }

    // create instance of database class
    $database = new Database();

    // create instance of match repository class
    $matchData = new MatchRepository($database);

    // get the match participants
    $match = $matchData->getMatch($_GET["id"]);

    // get the challenger of the match
    $challenger = $match[0]->getChallenger();

    // get the defender of the match
    $defender = $match[0]->getDefender();

    // if the match doesn't exist
    if (empty($challenger)) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the match doesn't exist
        $chops->message = 'This match doesn\'t exist';

        // set the back link
        $chops->backurl = 'viewmatch.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    }

    // get the match data
    $matchmoves = $matchData->getMatchData($_GET["id"]);

    // get the challenger's attack/defense pattern
    $challengerpattern = $matchmoves[0]->getChallengerMoves();

    // get the defense's attack/defense pattern
    $defenderpattern = $matchmoves[0]->getDefenderMoves();

    // split the challenger's array into individual attack and defense arrays
    $challengerpatternarray = explode(":", $challengerpattern);

    // split the defender's array into individual attack and defense arrays
    $defenderpatternarray = explode(":", $defenderpattern);

    // template variable
    $blow = Array();

    // number of times challenger has hit
    $challengerpoints = 0;

    // number of times defender has hit
    $defenderpoints = 1;

    // create instance of move utility class
    $move = new Move();

    for ($i = 0; $i < 4; $i++) {
        for ($j = 0; $j < 3; $j++) {
            // set the individual attack index for the array
            $attackIndex = $i % 2;

            // set the individual defense index for the array
            $defenseIndex = ($i + 1) % 2;

            // set the round
            $roundPos = ($i / 2 % 2) * 3 + $j;

            // if this is the first round
            if ($i % 2 == 0) {
                // get the challenger's individual attack from the array
                $challengerattack = (int)substr($challengerpatternarray[$attackIndex], $roundPos, 1);

                // get the defender's individual defense from the array
                $defenderdefense = (int)substr($defenderpatternarray[$defenseIndex], $roundPos, 1);

                // did the move hit?
                $isHit = (bool)$move->isHit($challengerattack, $defenderdefense);

                // if the move hit
                if ($isHit) {
                    // add a point for the challenger
                    $challengerpoints += 1;
                }

                // what type of hit was it
                $type = $move->process($challengerattack);

                // add the blow to the array
                $blow[] = $move->output($challenger, $isHit, $defender, $type);
            } // otherwise we are on the second round
            else {
                // get the challenger's individual defense from the array
                $challengerdefense = (int)substr($challengerpatternarray[$defenseIndex], $roundPos, 1);

                // get the defender's individual attack from the array
                $defenderattack = (int)substr($defenderpatternarray[$attackIndex / 2], $roundPos, 1);

                // did the move hit?
                $isHit = (bool)$move->isHit($defenderattack, $challengerdefense);

                // if the move hit
                if ($isHit) {
                    // add a point for the defender
                    $defenderpoints += 1;
                }

                // what type of hit was it
                $type = $move->process($defenderattack);

                // add the blow to the array
                $blow[] = $move->output($defender, $isHit, $challenger, $type);
            }
        }
    }

    // if the challenger has more points
    if ($challengerpoints > $defenderpoints) {
        // get the winner as the challenger
        $chops->user = $challenger;
    } // if the defender has more points
    elseif ($defenderpoints > $challengerpoints) {
        // get the winner as the defender
        $chops->user = $defender;
    } // otherwise both have the same amount of points
    else {
        // get the winner as the defender
        $chops->user = "no one";
    }

    // set the blow array
    $chops->MatchData = $blow;

    // and display it
    $chops->display('spar.tpl');

// and terminate
    exit;
}

?>