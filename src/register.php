<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration info
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user is logged in
if (isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the game
    header("Location: main.php");

    // and terminate
    exit;
} // if the form wasn't submited
elseif (!isset($_POST["submit"])) {
    // display it
    $chops->display('register.tpl');
} // otherwise the form was submitted
else {
    // if the antibot field was filled in
    if (!empty($_POST["antibot"])) {
        // set the dialog title
        $chops->title = 'Error';

        // sthe dialog message stating the username is empty
        $chops->message = 'Please make sure to leave the first field empty, it is a means of preventing bot registrations';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    }

    // if the username is empty
    if (empty($_POST["username"])) {
        // set the dialog title
        $chops->title = 'Error';

        // sthe dialog message stating the username is empty
        $chops->message = 'username is empty';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the username is too short or too big
    elseif (strlen($_POST["username"]) < 4 || strlen($_POST["username"]) > 16) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the required username length
        $chops->message = 'username must be 4-16 characters long';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } elseif (!preg_match("#^[a-z0-9]*$#", $_POST["username"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the username contains invalid characters
        $chops->message = 'username contains invalid characters';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // otherwise the username is filled in
    else {
        // so sanitize it
        $username = $_POST["username"];
    }

    // if the email is empty
    if (empty($_POST["email"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the email is empty
        $chops->message = 'email is empty';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the email is invalid
    elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $_POST["email"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the email is invalid
        $chops->message = 'the email address you entered is invalid';

        // set the back url
        $chops->backurl = 'register.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // otherwise the email is ok
    else {
        // so sanitize it
        $email = $_POST["email"];
    }

    // open a connection to the database
    $db = new Database();

    // create an instance of the user class
    $user = new User($db);

    // check to see if the username already exists
    if ($user->existsUsername($username)) {
        // set the dialog title
        $chops->title = 'Error';

        // if the username already excists display an error stating it
        $chops->message = 'that username already exists';

        // set the back url
        $chops->backurl = 'register.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit();
    }

    // check to see if the email already is already in use
    if (REQUIRE_UNIQUE_EMAIL && $user->isInUseEmail($email)) {
        // set the dialog title
        $chops->title = 'Error';

        // the email is already in use so display an error stating it
        $chops->message = 'that email is already in use';

        // set the back url
        $chops->backurl = 'register.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit();
    }

    // check to see if the ip already is already in use
    if (REQUIRE_UNIQUE_IP && $user->isInUseIP($_SERVER["REMOTE_ADDR"])) {
        // set the dialog title
        $chops->title = 'Error';

        // the ip is already in use so display an error stating it
        $chops->message = 'An account with your ip already excists';

        // set the back url
        $chops->backurl = 'register.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit();

    }

    // generate a temporary random password
    $password = $user->generateRandomPassword(8);

    // attempt to add the user
    if (!$user->add($username, $password, $email)) {
        // set the dialog title
        $chops->title = 'Error';

        // if everything checks out display a success page
        $chops->message = 'Could not add user, contact an admin';

        // set the back url
        $chops->backurl = 'register.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit();
    }

    // email the user his temporary password
    if (!$user->email("Your Temporary Password",
        "Thank Your for registering, your temporary password is: " . $password . ". If you didn't register then please disregard this message.")
    ) {
        // set the dialog title
        $chops->title = 'Error';

        // if everything checks out display a success page
        $chops->message = 'Could not send temporary password to user, contact an admin';

        // set the back url
        $chops->backurl = 'index.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit();

    }

    // set the dialog title
    $chops->title = 'Info';

    // set the dialog message stating so
    $chops->message = 'Your registration was successful, your password was sent to the email address you provided during registration.';

    // set the back url
    $chops->backurl = 'index.php';

    // display it
    $chops->display('messagedialog.tpl');

    // and terminate
    exit();
}

?>