-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 08, 2014 at 08:58 PM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Table structure for table `ae_matches`
--

CREATE TABLE IF NOT EXISTS `ae_matches` (
  `id` int(11) NOT NULL,
  `challengermoveset` varchar(13) NOT NULL,
  `defendermoveset` varchar(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ae_matchesindex`
--

CREATE TABLE IF NOT EXISTS `ae_matchesindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenger` varchar(16) NOT NULL,
  `defender` varchar(16) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ae_users`
--

CREATE TABLE IF NOT EXISTS `ae_users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(60) NOT NULL,
  `salt` varchar(22) NOT NULL,
  `email` varchar(64) NOT NULL,
  `notificationpreference` tinyint(1) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `validationkey` varchar(32) NOT NULL,
  `xp` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ae_users`
--

INSERT INTO `ae_users` (`id`, `username`, `password`, `salt`, `email`, `notificationpreference`, `ip`, `validationkey`, `xp`) VALUES
(1, 'admin', '$2a$12$U60maoQMyigOSUoL0IlDMeMW8dERnFDVsGjqeAB4W1qRRtNHbi6KS', 'U60maoQMyigOSUoL0IlDMp', 'admin@example.com', 1, '127.0.0.1', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
