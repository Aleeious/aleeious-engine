<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {
    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} elseif (!isset($_POST["submit"])) {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    $chops->display("changenotificationpreferences.tpl");
} else {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    // if the site code was changed
    if ($_POST["preference"] < 0 || $_POST["preference"] > 1) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating that tampering was detected
        $chops->message = 'Tampering Detected. Please don\'t do that!';

        // set the back url
        $chops->backurl = 'profile.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } else {
        // create instance of database class
        $database = new Database();

        // create instance of user class
        $user = new User($database);

        if (!$user->changeNotificationPreference($_SESSION["username"], $_POST["preference"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the feature isn't implemented yet
            $chops->message = 'An error occured while connecting to the database. Contact an admin.';

            // set the back url
            $chops->backurl = 'profile.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } else {
            // set the dialog title
            $chops->title = 'Info';

            // set the dialog message stating the feature isn't implemented yet
            $chops->message = 'Your notification preferences have been updated';

            // set the back url
            $chops->backurl = 'profile.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }
    }
}

?>