<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user is logged in
if (isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the game
    header("Location: main.php");

    // and terminate
    exit;
} // if the form wasn't submitted
elseif (!isset($_POST["submit"])) {
    // display it
    $chops->display('forgotusername.tpl');
} // otherwise the form was submitted
else {
    // if the email is empty
    if (empty($_POST["email"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the email is empty
        $chops->message = 'email is empty';

        // set the back url
        $chops->backurl = 'forgotusername.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the email is invalid
    elseif (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $_POST["email"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the email is invalid
        $chops->message = 'the email address you entered is invalid';

        // set the back url
        $chops->backurl = 'forgotusername.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // otherwise the email is ok
    else {
        // so sanitize it
        $_POST["email"] = trim(strip_tags($_POST["email"]));
    }

    // create instance of database class
    $database = new Database();

    // create instance of user class
    $user = new User($database);

    // check the users email
    if (!$user->retrieveUsername($_POST["email"])) {
        // set the dialog message
        $chops->title = 'Error';

        // if the username couldn't be retrieved, a database error occured so display an error
        $chops->message = 'there was a problem contacting the database, please notify an admin';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } else {
        // if the query was successful and a username was found
        if ($user->getUsername()) {
            // attempt to email the user his username
            if ($user->email("Your Requested Username",
                "Someone possibly you has asked to have your username sent. Your username is: " . $user->getUsername() . ". If you did not ask to have your username sent, please contact the admin.")
            ) {
                // set the dialog title
                $chops->title = 'Info';

                // otherwise display the normal message
                $chops->message = 'If your provided correct info your username will be mailed';

                // set the back url
                $chops->backurl = 'index.php';

                // display it
                $chops->display('messagedialog.tpl');

                // and terminate
                exit;
            } else {
                // set the dialog title
                $chops->title = 'Error';

                // otherwise display the error messgae
                $chops->message = 'The username could not be emailed. Contact an admin';

                // set the back url
                $chops->backurl = 'index.php';

                // display it
                $chops->display('messagedialog.tpl');

                // and terminate
                exit;
            }
        } // otherwise an incrrect email was provided, but display the normal message to prevent brute force
        else {
            // set the dialog title
            $chops->title = 'Info';

            // otherwise display the normal messgae
            $chops->message = 'If your provided correct info your username will be mailed';

            // set the back url
            $chops->backurl = 'index.php';

            // display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }


    }
}

?>