<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// display it
$chops->display('privacypolicy.tpl');

// and terminate
exit;

?>