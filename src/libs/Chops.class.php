<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class for loading and displaying templates
 */
class Chops
{
    /** @var Array $variables template variables */
    private $variables;

    /** @var String $templateDirectory template directory */
    private $templateDirectory;

    /**
     * class constructor
     * @param String $directory template directory
     * @throws Exception
     */
    public function __construct($directory = "templates")
    {
        // if the directory doesn't excist
        if (!is_dir(realpath($directory))) {
            // throw an Exception
            throw new Exception("Directory $this->templateDirectory doesn't excist");
        } // if the directory can't be read
        elseif (!is_readable($directory)) {
            throw new Exception("Directory $this->templateDirectory does not have read privilages.");
        }

        // set the template directory
        $this->templateDirectory = $directory;
    }

    /**
     * sets a template variable
     * @param String $key the key to assign to value to
     * @param Mixed $value the value of the key
     */
    public function __set($key, $value)
    {
        $this->variables[$key] = $value;
    }

    /**
     * displays the template
     * @param String $file filename of template to display
     * @return String $output the completed template file
     * @throws Exception
     */
    public function display($file)
    {
        // build the file path
        $templateFile = realpath($this->templateDirectory) . '/' . basename($file);

        // if the template file doesn't exist
        if (!file_exists($templateFile)) {
            // throw an exception stating the file could not be loaded
            throw new Exception("Could not load template file: $file in $this->templateDirectory");
        } elseif (!is_readable($templateFile)) {
            throw new Exception("The template file $file in directory $this->templateDirectory not have read privilages.");
        }

        // if there are variables to extract
        if (!is_null($this->variables)) {
            /* extract all the variables */
            extract($this->variables);
        }

        // if ob_start hasn't already been started
        if (!in_array("ob_gzhandler", ob_list_handlers())) {
            // if the contents haven't been compressed yet
            while (ob_get_level() === 0) {
                // then start it
                ob_start("ob_gzhandler");
            }
        } // otherwise ob_start has been started
        else {
            // start so resume it
            ob_start();
        }

        // load the template once
        include_once($templateFile);

        // get the output from the buffer
        $output = ob_get_contents();

        // return the resulting output
        return $output;
    }
}

?>