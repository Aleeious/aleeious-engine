<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once('Match.class.php');

/**
 * Match Repository for retrieval of match data for later storage
 */
class MatchRepository
{
    /** @const MATCH_STATUS_OPEN constant for open Match */
    const MATCH_STATUS_OPEN = 0;

    /** @const MATCH_STATUS_COMPLETED constant for completed Match */
    const MATCH_STATUS_COMPLETED = 1;

    /** @const MATCH_STATUS_CANCELLED constant for cancelled Match */
    const MATCH_STATUS_CANCELLED = 2;

    /** constant MATCH_STATUS_ERROR for match that was result of error */
    const MATCH_STATUS_ERROR = 3;

    /** @const MATCH_TYPE_HOME constant for home matches */
    const MATCH_TYPE_HOME = 0;

    /** MATCH_TYPE_AWAY constant for away matches */
    const MATCH_TYPE_AWAY = 1;

    /** @const MATCH_TYPE_ALL constant for all matches */
    const MATCH_TYPE_ALL = 2;

    /** @var Database $db instance of database */
    protected $db;

    /**
     * class constructor
     * @param $db database class instance
     */
    public function __construct(Database $db)
    {
        // get instance of database
        $this->db = $db;
    }

    /**
     * adds the attack pattern from the challenger to the database
     * @param String $pattern challenger's attack/block pattern as attack:block
     * @return Boolean true on success, otherwise false
     */
    public function add($pattern)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('INSERT INTO ' . TABLE_MATCHES . ' SET challengermoveset = ?, defendermoveset = ?');

        // bind_param only accepts variables so create a variable to hold an empty string
        $empty = "";

        // bind the variables
        $statement->bind_param('ss', $pattern, $empty);

        // if the statement executed successfully
        if ($statement->execute()) {
            // fetch the results
            $statement->fetch();

            // if the match data was added successfully
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were updated
            return false;
        }

        // otherwise the user couldn't be added so return false
        return false;
    }

    /**
     * adds a match index to the database
     * @param String $challenger the person initiating the challenge
     * @param String $defender the person being challenged
     * @return Boolean true on success, otherwise returns false
     */
    public function addIndex($challenger, $defender)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('INSERT INTO ' . TABLE_MATCHESINDEX . ' SET challenger = ?, defender = ?, status = ?');

        // convert constant to variable
        $status = self::MATCH_STATUS_OPEN;

        // bind the variables
        $statement->bind_param('ssi', $challenger, $defender, $status);

        // if the statement executed successfully
        if ($statement->execute()) {
            // fetch the results
            $statement->fetch();

            // if the match metadata was added successfully
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were added
            return false;
        }

        // otherwise the query failed so return false
        return false;
    }

    /**
     * changes the status of a match
     * @param Int $id match id to change
     * @param String $username username to change status for
     * @param Int $newstatus new match status to change to
     * @param Int match type (Home/Away)
     * @return Boolean true on success otherwise false
     */
    public function changeStatus($id, $username, $newstatus, $type)
    {
        if ($type == self::MATCH_TYPE_HOME) {
            // prepare the sql statement to change the match status
            $statement = $this->db->prepare("UPDATE " . TABLE_MATCHESINDEX . " SET `status` = ? WHERE `id` = ? AND `challenger` = ? LIMIT 1");
        } elseif ($type == self::MATCH_TYPE_AWAY) {
            // prepare the sql statement to change the match status
            $statement = $this->db->prepare("UPDATE " . TABLE_MATCHESINDEX . " SET `status` = ? WHERE `id` = ? AND `defender` = ? LIMIT 1");
        }

        // bing the variables
        $statement->bind_param('iis', $newstatus, $id, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were updated
            return false;
        }

        // the sql statement failed
        return false;
    }

    /**
     * counts the number of matches a player has based on parameters
     * @param String $username username to check number of pending challenges
     * @param Int $type match type 0 = Home, 1 = Away and 2 = All
     * @param Int $status Status of match 0 = open, 1 = completed
     * @return Int number of pending matches for user
     */
    public function getCount($username, $type, $status)
    {
        if ($type == self::MATCH_TYPE_HOME) {
            // prepare the sql statement to return all home matches
            $statement = $this->db->prepare("SELECT count(id) as 'count' FROM " . TABLE_MATCHESINDEX . " WHERE status = ? AND challenger = ? LIMIT 5");

            // bind the variables
            $statement->bind_param('is', $status, $username);
        } elseif ($type == self::MATCH_TYPE_AWAY) {
            // prepare the sql statement to return all away matches
            $statement = $this->db->prepare("SELECT count(id) as 'count' FROM " . TABLE_MATCHESINDEX . " WHERE status = ? AND defender = ? LIMIT 5");

            // bind the variables
            $statement->bind_param('is', $status, $username);
        } elseif ($type == self::MATCH_TYPE_ALL) {
            // prepare the sql statement to return all matches
            $statement = $this->db->prepare("SELECT count(id) as 'count' FROM " . TABLE_MATCHESINDEX . " WHERE challenger = ? AND status = ? OR defender = ? AND status = ? LIMIT 5");

            // bind the variables
            $statement->bind_param('sisi', $username, $status, $username, $status);
        }

        // if the statement executed successfully
        if ($statement->execute()) {
            // store the result
            $result = $statement->get_result();

            // get the number of results
            $totalPendingMatches = $result->fetch_assoc();

            // return the total number of pending matches
            return $totalPendingMatches['count'];
        }

        // query didn't execute so return false
        return false;
    }

    /**
     * get the requested match of the player by id
     * @param Int $id match id to return match info for
     * @param the number of matches to return
     * @return Array matches as instance of match class
     */
    public function getMatch($id)
    {
        // prepare the sql statement to get a single home match
        $statement = $this->db->prepare("SELECT challenger, defender FROM " . TABLE_MATCHESINDEX . " WHERE id = ? AND status = ? LIMIT 1");

        // convert constant to variable
        $status = self::MATCH_STATUS_OPEN;

        // bing the variables
        $statement->bind_param('ii', $id, $status);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($challenger, $defender);

            // fetch all the results
            $statement->fetch();

            // return a single home match as a match object array
            $results[] = new Match($id, $challenger, $defender);

            // so matches were found so return true
            return $results;
        }

        // the query failed so return false
        return false;
    }

    /**
     * get the current matches the player has initiated
     * @param String $username username to check for matches for
     * @param Int $type match type 0 = Home, 1 = Away
     * @param Int $limit the number of matches to return
     * @return Array matches as instance of match class
     */
    public function getMatches($username, $type, $status, $limit)
    {
        if ($type == self::MATCH_TYPE_HOME) {
            // prepare the sql statement to get all home matches
            $statement = $this->db->prepare("SELECT id, defender FROM " . TABLE_MATCHESINDEX . " WHERE challenger = ? AND status = ? LIMIT " . $limit);
        } elseif ($type == self::MATCH_TYPE_AWAY) {
            // prepare the sql statement to change the password
            $statement = $this->db->prepare("SELECT id, challenger FROM " . TABLE_MATCHESINDEX . " WHERE defender = ? AND status = ? LIMIT " . $limit);
        }

        // bing the variables
        $statement->bind_param('si', $username, $status);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($id, $entity);

            // fetch all the results
            while ($statement->fetch()) {
                // if all home matches were requested
                if ($type == self::MATCH_TYPE_HOME) {
                    // return all home matches as an array of match objects
                    $results[] = new Match($id, $username, $entity);
                } // otherwise all away matches were requested
                elseif ($type == self::MATCH_TYPE_AWAY) {
                    // return all away matches as an array of match objects
                    $results[] = new Match($id, $entity, $username);
                }
            }

            // so matches were found so return true
            return $results;
        }

        // the query failed so return false
        return false;
    }

    /**
     * get the requested match patterns of the match by id
     * @param Int $id match id to return match patterns for
     * @return Array matches as instance of match class
     */
    public function getMatchData($id)
    {
        // prepare the sql statement to get a single home match
        $statement = $this->db->prepare("SELECT challengermoveset, defendermoveset FROM " . TABLE_MATCHES . " WHERE id = ? LIMIT 1");

        // convert constant to variable
        $status = self::MATCH_STATUS_COMPLETED;

        // bing the variables
        $statement->bind_param('i', $id);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($challengermoveset, $defendermoveset);

            // fetch all the results
            $statement->fetch();

            // return a single home match as a match object array
            $results[] = new Match($id, "", "", $challengermoveset, $defendermoveset);

            // so matches were found so return true
            return $results;
        }

        // the query failed so return false
        return false;
    }

    /**
     * adds defender movesset after matach is accepted
     * @param Int $id id match id to update
     * @param String $pattern defenders attack/block pattern
     * @return true on success, otherwise returns false
     */
    public function updateMatch($id, $pattern)
    {
        // prepare the sql statement
        $statement = $this->db->prepare("UPDATE " . TABLE_MATCHES . " SET `defendermoveset` = ? WHERE `id` = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('si', $pattern, $id);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // otherwise return false
            return false;
        }

        // the statement executed but the tables couldn't be unrolled so return false
        return false;
    }
}

?>