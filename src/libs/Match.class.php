<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class for storing match data for retrieval
 */
class Match
{
    /** @var Int $id match id */
    private $id;

    /** @var String $challenger challenger of match */
    private $challenger;

    /** @var String $defender defender of match */
    private $defender;

    /** @var String $challegermoveset challenger's attack/defense moves */
    private $challengermoveset;

    /** @var String $challegermoveset challenger's attack/defense moves */
    private $defendermoveset;

    /**
     * class constructor
     * @param Int $id match id
     * @param String $challenger challenger of the match
     * @param String $defender defender of the match
     */
    public function __construct($id, $challenger, $defender, $challengermoveset = "", $defendermoveset = "")
    {
        $this->id = $id;
        $this->challenger = $challenger;
        $this->defender = $defender;
        $this->challengermoveset = $challengermoveset;
        $this->defendermoveset = $defendermoveset;
    }

    /**
     * returns match id
     * @return match id
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * returns challenger
     * @return String challenger
     */
    public function getChallenger()
    {
        return $this->challenger;
    }

    /**
     * returns defender
     * @return String defender
     */
    public function getDefender()
    {
        return $this->defender;
    }

    /**
     * returns challenger's move set
     * @return String challenger pattern
     */
    public function getChallengerMoves()
    {
        return $this->challengermoveset;
    }

    /**
     * returns defender's move set
     * @return String defender pattern
     */
    public function getDefenderMoves()
    {
        return $this->defendermoveset;
    }
}

?>