<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class for easy management of user related functions
 */
class User
{
    /** @var Database $db database instance */
    protected $db;

    /** @var String $username username */
    private $username;

    /** @var String $password user's password */
    private $password;

    /** @var String $validationkey validation key */
    private $validationkey;

    /** @var String $email user's email */
    private $email;

    /** @var Boolean $notificationpreference user's notification preference */
    private $notificationpreference;

    /** @var Int $xp user's xp */
    private $xp;

    /**
     * class constructor
     * @param Database $db instance of database class
     */
    public function __construct(Database $db)
    {
        // get instance of database
        $this->db = $db;
    }

    /**
     * @return String user's username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return String user's password hash
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return String user's validation key
     */
    public function getValidationKey()
    {
        return $this->validationkey;
    }

    /**
     * @return String user's email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Boolean notification preference
     */
    public function getNotificationPreference()
    {
        return $this->notificationpreference;
    }

    /**
     * @returns Int player's experience points
     */
    public function getXP()
    {
        return $this->xp;
    }

    /**
     * adds the user to the database
     * @param String $username of user to add
     * @param String $password of user to add
     * @param String $email address of user to add
     * @return Boolean true on success, otherwise false
     */
    public function add($username, $password, $email)
    {
        // generate a random salt
        $salt = $this->generateSalt();

        // generate the password hash
        $passwordhash = $this->generateHash($password, $salt);

        // prepare the sql statement
        $statement = $this->db->prepare("INSERT INTO " . TABLE_USERS . " SET `username` = ?, `password` = ?, `salt` = ?, `email` = ?, `notificationpreference` = ?, `ip` = ?, `validationkey` = ?, `xp` = ?");

        // assign a temporary variable for zero
        $zero = 0;

        // assign a temporary variable for empty
        $empty = "";

        // bing the variables
        $statement->bind_param('ssssissi', $username, $passwordhash, $salt, $email, $zero, $_SERVER['REMOTE_ADDR'],
            $empty, $zero);

        // if the statement executed successfully
        if ($statement->execute()) {
            // fetch the results
            $statement->fetch();

            // if the user was added successfully
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were added
            return false;
        }

        // otherwise the user couldn't be added so return false
        return false;
    }

    /**
     * changes the users notification preference
     * @param String $username the username to change the preference of
     * @param Int $newpreference the user's new preference
     * @return Boolean true on success otherwise false
     */
    public function changeNotificationPreference($username, $newpreference)
    {
        // prepare the sql statement to change the password
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `notificationpreference` = ? WHERE username = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('ss', $newpreference, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // return success
            return true;
        }

        // the sql statement failed
        return false;
    }

    /**
     * changes the users password
     * @param String $username username to change password for
     * @param String $password new password to change to
     * @return Boolean true on success otherwise false
     */
    public function changePassword($username, $password)
    {
        // generate a new salt
        $salt = $this->generateSalt();

        // hash the password using the stored hash
        $passwordhash = $this->generateHash($password, $salt);

        // prepare the sql statement to change the password
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `salt` = ?, `password` = ? WHERE username = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('sss', $salt, $passwordhash, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were updated
            return false;
        }

        // the sql statement failed
        return false;
    }

    /**
     * checks the password change request
     * @param String $username username to change password for
     * @param String $uniqueid unique id for change request
     * @return Boolean true on success otherwise false
     */
    public function checkPasswordChangeRequest($username, $uniqueid)
    {

        // prepare the sql statement to change the password
        $statement = $this->db->prepare("SELECT email FROM " . TABLE_USERS . " WHERE username = ? and validationkey = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('ss', $username, $uniqueid);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($result);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($result) {
                $this->email = $result;

                // return success
                return true;
            }
        }

        // otherwise the request couldn't be fulfilled so return false
        return false;
    }

    /**
     * checks the users login credentials
     * @param String $username username to check
     * @param String $password user password to check
     * @return Boolean true if login is successful otherwise false
     */
    public function checkLogin($username, $password)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT password, salt FROM ' . TABLE_USERS . ' WHERE USERNAME = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($result, $resultsalt);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($result) {
                // hash the password using the stored hash
                $passwordhash = $this->generateHash($password, $resultsalt);

                // if the password matches
                if (substr($result, 0, 60) == substr($passwordhash, 0, 60)) {
                    // set the username
                    $this->username = $username;

                    // set the password
                    $this->password = $passwordhash;

                    // return true
                    return true;
                } // otherwise the password is incorrect
                else {
                    // return false
                    return false;
                }
            }
        }

        // otherwise the query failed to execute so return false
        return false;
    }

    /**
     * returns a users notification preference
     * @param String $username the username to check the notification preference of
     * @return Boolean true on success, otherwise false
     */
    public function checkNotificationPreference($username)
    {
        // prepare the sql statement to change the password
        $statement = $this->db->prepare("SELECT notificationpreference FROM " . TABLE_USERS . " WHERE username = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('s', $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($result);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($result) {
                $this->notificationpreference = $result;

                // return success
                return true;
            }
        }

        // otherwise return false
        return false;
    }

    /**
     * clears the users password change request
     * @param String $username username to clear the change request for
     * @return Boolean true on success otherwise false
     */
    public function clearChangeRequest($username)
    {
        // generate a new unique id
        $uniqueid = $this->generateRandomPassword(32);

        // prepare the sql statement to check if the request is valid
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `validationkey` = ? WHERE username = ? LIMIT 1");

        // bind the variables
        $statement->bind_param('ss', $uniqueid, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // return success
                return true;
            }

            // no rows were updated
            return false;
        }

        // sql statement failed
        return false;
    }

    /**
     * emails the user
     * @param String $subject email subject
     * @param String $message the email message
     * @return Boolean true on success, otherwise false
     */
    public function email($subject, $message)
    {
        // if the mail was sent successfully
        if (mail($this->email, $subject, $message, "From: " . ADMIN_EMAIL)) {
            // return success
            return true;
        }

        // otherwise the mail function failed so return failure
        return false;
    }

    /**
     * checks if the user already exists in the database
     * @param String $username username of user to check
     * @returns Boolean true if username exists, otherwise false
     */
    public function existsUsername($username)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT id FROM ' . TABLE_USERS . ' WHERE username = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($row);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($row) {
                // return true
                return true;
            }
        }

        // otherwise the user doesn't exist so return false
        return false;
    }

    /**
     * Generates a hash
     * @param String $password password to hash
     * @param String $salt salt for the crypt function
     * @param Int $rounds number of times to perform encryption
     * @return String hashed password
     */
    public function generateHash($password, $salt, $rounds = 12)
    {
        // return the password hash using crypt
        return crypt($password, sprintf('$2a$%02d$', $rounds) . $salt);
    }

    /**
     * generates a password change request
     * @param String $username username to change password for
     * @param String $email email to change password for
     * @return Boolean true on success otherwise false
     */
    public function generateChangeRequest($username, $email)
    {
        // generate a new unique id
        $uniqueid = $this->generateRandomPassword(32);

        // prepare the sql statement to change the password
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `validationkey` = ? WHERE username = ? and email = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('sss', $uniqueid, $username, $email);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // set the email address
                $this->email = $email;

                // set the validation key field
                $this->validationkey = $uniqueid;
            }

            // the query was successful
            return true;
        }

        // the sql query failed or no user was found
        return false;
    }

    /**
     * Generates a random password
     * @param Int $length length of generated password
     * @return String generated random password
     */
    public function generateRandomPassword($length)
    {
        // list the characters valid in the salt
        $validcharacters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));

        // password
        $password = '';
        // repeat the loop 22 times cause the salt is 22 characters
        for ($i = 0; $i < $length; $i++) {
            // generate the salt
            $password .= $validcharacters[array_rand($validcharacters)];
        }

        // return the new password
        return $password;
    }

    /**
     * Generates a salt
     * @return String generated salt
     */
    public function generateSalt()
    {
        // list the characters valid in the salt
        $validcharacters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));

        // holds the salt
        $salt = "";

        // repeat the loop 22 times cause the salt is 22 characters
        for ($i = 0; $i < 22; $i++) {
            // generate the salt
            $salt .= $validcharacters[array_rand($validcharacters)];
        }

        // return the password hash using crypt
        return $salt;
    }

    /**
     * gets user info and populates it
     * @param String $username username of user to retrieve info of
     * @return Boolean true on success, otherwise false
     */
    public function getInfo($username)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT email, notificationpreference, xp FROM ' . TABLE_USERS . ' WHERE USERNAME = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($email, $notificationprefernce, $xp);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($email) {
                $this->email = $email;
                $this->notificationpreference = $notificationprefernce;
                $this->xp = $xp;
            }

            // return success
            return true;
        }

        // query failed so return false
        return false;
    }

    /**
     * increments the users experience
     * @param String $username the username to change the preference of
     * @param Int $amount the amount to increment the user's xp
     * @return Boolean true on success otherwise false
     */
    public function incrementXP($username, $amount)
    {
        // prepare the sql statement to change the password
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `xp` = 'xp' + ? WHERE username = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('is', $amount, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            // return success
            return true;
        }

        // the sql statement failed
        return false;
    }

    /**
     * checks if the user already exists in the database
     * @param String $email email of user to check
     * @returns Boolean true if email is in use, otherwise false
     */
    public function isInUseEmail($email)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT id FROM ' . TABLE_USERS . ' WHERE email = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $email);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($row);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($row) {
                // return true
                return true;
            }
        }

        // otherwise the query failed or the email isn't in use return false
        return false;
    }

    /**
     * checks if the ip already is associated with a user
     * @param String $ip ip of user to check
     * @returns Boolean true if ip exists, otherwise false
     */
    public function isInUseIP($ip)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT id FROM ' . TABLE_USERS . ' WHERE ip = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $ip);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($row);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($row) {
                // return true
                return true;
            }
        }

        // otherwise the query failed or the ip isn't in use so return false
        return false;
    }

    /**
     * changes the users password
     * @param String $username username to check
     * @return Boolean true if login is successful otherwise false
     */
    public function resetPassword($username)
    {
        // generate a new password
        $password = $this->generateRandomPassword(12);

        // generate a random salt
        $salt = $this->generateSalt();

        // hash the password using the stored hash
        $passwordhash = $this->generateHash($password, $salt);

        // prepare the sql statement to change the password
        $statement = $this->db->prepare("UPDATE " . TABLE_USERS . " SET `salt` = ?, `password` = ? WHERE username = ? LIMIT 1");

        // bing the variables
        $statement->bind_param('sss', $salt, $passwordhash, $username);

        // if the statement executed successfully
        if ($statement->execute()) {
            if ($this->db->affected_rows) {
                // save new password
                $this->password = $password;

                // return success
                return true;
            }
        }

        // the query failed to execute
        return false;
    }

    /**
     * retrieves the users username
     * @param String $email user email tied to user
     * @return Boolean true if username is found otherwise false
     */
    public function retrieveUsername($email)
    {
        // prepare the sql statement
        $statement = $this->db->prepare('SELECT username FROM ' . TABLE_USERS . ' WHERE email = ? LIMIT 1');

        // bing the variables
        $statement->bind_param('s', $email);

        // if the statement executed successfully
        if ($statement->execute()) {
            // get the number of results
            $statement->bind_result($result);

            // fetch the results
            $statement->fetch();

            // if a result exists
            if ($result) {
                // set email
                $this->email = $email;

                // set username
                $this->username = $result;

                // return true
                return true;
            } else {
                // no user exists by that username but the query executed
                return true;
            }
        } else {
            // the sql statement didn't execute
            return false;
        }
    }

    /**
     * sanitizes user info
     */
    public function sanitize()
    {
        // clean all user info
        $this->username = "";
        $this->password = "";
        $this->email = "";
    }
}

?>