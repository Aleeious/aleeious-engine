<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * class for managing processing moves
 */
class Move
{
    /**
     * returns if the move hit
     * @param int $attack player's attack
     * @param int $defense player's defense
     * @return bool true if attack was successful, otherwise false
     */
    function isHit($attack, $defense)
    {
        return ($attack <> $defense);
    }

    /**
     * adds move to blow array
     * @param int $type move type
     * @return String attack type
     */
    public function process($type)
    {
        switch ($type) {
            case 1:
                return "leg";
                break;
            case 2:
                return "chest";
                break;
            case 3:
                return "head";
                break;
        }
    }

    /**
     * formats the output
     * @param String $attacker attacker's username
     * @param bool $isHit if move was blocked
     * @param String $defender defender's username
     * @param Int $type the type of attack performed
     * @return String formatted output
     */
    public function output($attacker, $isHit, $defender, $type)
    {
        // if the defender hit the attacker
        if ($isHit) {
            // format the string for a successful attack
            $output = $defender . " hits " . $attacker . " in the ";
        } // otherwise the attack was blocked
        else {
            // format the string for a block
            $output = $defender . " blocks " . $attacker . " attack to the ";
        }

        return $output = $output . $type;
    }
}

?>