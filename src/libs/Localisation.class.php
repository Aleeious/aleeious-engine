<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class for translating string
 */
class Localisation
{
    /** @var Array $translations word translations */
    private $translations;

    /** @var String $languageDirectory language directory */
    private $languageDirectory;

    /**
     * Class constructor
     * @param String $directory language files directory
     * @throws exception
     */
    public function __construct($directory = "lang")
    {
        // if the directory doesn't exist
        if (!is_dir(realpath($directory))) {
            // throw an Exception
            throw new Exception("Directory $this->languageDirectory doesn't exist");
        } // if the directory can't be read
        elseif (!is_readable($directory)) {
            throw new Exception("Directory $this->languageDirectory does not have read privileges.");
        }

        // set the template directory
        $this->languageDirectory = $directory;
    }

    /**
     * gets a language translation
     * @param String $key the key of the word to return the value off
     * @return String $value the value of the translated work
     */
    public function __get($key)
    {
        return $this->translations[$key];
    }

    function localize($filename) {
        /* If the $translations array is empty load the language file */
        if (is_null($this->translations)) {
            // build the file path
            $languageFile = realpath($this->languageDirectory) . '/' . basename($filename) . ".txt";
            // if the language file isn't found
            if (!file_exists($languageFile)) {
                throw new Exception("Could not load language file: $filename in $this->languageDirectory");
            }
            // if the language file isn't readable
            elseif (!is_readable($languageFile)) {
                throw new Exception("The language file $filename in directory $this->languageDirectory not have read privileges.");
            }
            // extract the translation strings
            $languageFileContents = file_get_contents($languageFile);

            /* Load the language file as a JSON object and transform it into an associative array */
            $this->translations = json_decode($languageFileContents, true);
        }
        // return if any errors occurred
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
?>