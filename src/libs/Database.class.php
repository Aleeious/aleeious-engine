<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// import configuration data
require_once('./config.php');

/**
 * class for managing database connections
 */
class Database extends mysqli
{
    /**
     * class constructor
     * @throws Exception
     */
    public function __construct()
    {
        // turn on error reporting
        mysqli_report(MYSQLI_REPORT_ERROR);

        parent::__construct(DATABASE_SERVER, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME, DATABASE_SERVER_PORT);

        // if the connection couldn't be established
        if (mysqli_connect_errno()) {
            // throw an exception
            throw new exception(mysqli_connect_error(), mysqli_connect_errno());
        }
    }

    // creates the match table
    public function createMatchTable()
    {
        // prepare the sql statement to get a single home match
        $statement = $this->prepare("CREATE TABLE IF NOT EXISTS " . TABLE_MATCHES . "( `id` int(11) NOT NULL AUTO_INCREMENT, `challengermoveset` varchar(13) NOT NULL, `defendermoveset` varchar(13) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");

        // if the statement executed successfully
        return $statement->execute();
    }

    // creates the match table
    public function createMatchIndexTable()
    {
        // prepare the sql statement to get a single home match
        $statement = $this->prepare("CREATE TABLE IF NOT EXISTS " . TABLE_MATCHESINDEX . " ( `id` int(11) NOT NULL AUTO_INCREMENT, `challenger` varchar(16) NOT NULL, `defender` varchar(16) NOT NULL, `status` int(1) NOT NULL, PRIMARY KEY (`id`), KEY `id` (`id`), KEY `challenger` (`challenger`), KEY `defender` (`defender`), KEY `status` (`status`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");

        // if the statement executed successfully
        return $statement->execute();
    }

    // creates the match table
    public function createUserTable()
    {
        // prepare the sql statement to get a single home match
        $statement = $this->prepare("CREATE TABLE IF NOT EXISTS " . TABLE_USERS . " ( `id` int(8) NOT NULL AUTO_INCREMENT, `username` varchar(16) NOT NULL, `password` varchar(60) NOT NULL, `salt` varchar(22) NOT NULL, `email` varchar(64) NOT NULL, `notificationpreference` tinyint(1) NOT NULL, `ip` varchar(15) NOT NULL, `validationkey` varchar(32) NOT NULL, `xp` int(8) NOT NULL, PRIMARY KEY (`id`), KEY `password` (`password`), KEY `username` (`username`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");

        // if the statement executed successfully
        return $statement->execute();
    }
}

?>