<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * auto loads a class
 * @param String $className Name of class to autoload
 * @return Bool true on success, false on failure
 */
function __autoload($className)
{
    // if the class file exists
    if (file_exists('libs/' . $className . '.class.php')) {
        // load the class
        require('libs/' . $className . '.class.php');

        // return true
        return true;
    }

    // otherwise the class couldn't be loaded so return false
    return false;
}

?>