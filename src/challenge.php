<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} // otherwise if the form hasn't been submitted
else {
    if (!isset($_POST["submit"])) {
        // update the session timeout timer
        $_SESSION['lastactivity'] = time();

        // display it
        $chops->display('challenge.tpl');

        // and terminate
        exit;
    } // otherwise the form has been submitted

    else {
        // update the session timeout timer
        $_SESSION['lastactivity'] = time();

        // if the defender field is empty
        if (empty($_POST["defender"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set dialog message stating the defender field is empty
            $chops->message = 'defender is empty';

            // set the back url
            $chops->backurl = 'challenge.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // if the user entered his own name as the challenger
        else {
            if ($_POST["defender"] == $_SESSION["username"]) {
                // set the dialog title
                $chops->title = 'Error';

                // set dialog message stating the defender field is empty
                $chops->message = 'you cannot challenge yourself';

                // set the back url
                $chops->backurl = 'challenge.php';

                // and display it
                $chops->display('messagedialog.tpl');

                // and terminate
                exit;
            } else {
                // create instance of database class
                $database = new Database();

                // create instance of user
                $user = new User($database);

                // if the user doesn't exist
                if (!$user->existsUsername($_POST["defender"])) {
                    // set the dialog message title
                    $chops->title = 'Error';

                    // set the dialog message
                    $chops->message = 'The user you are trying to challenge doesn\'t exist. Make sure you have typed his name correctly and try again';

                    // set the back url
                    $chops->backurl = 'main.php';

                    // display it
                    $chops->display('messagedialog.tpl');

                    // and terminate
                    exit;
                }

                // string to hold attack values
                $attacks = "";

                // string to hold block values
                $blocks = "";

                for ($a = 1; $a <= 6; $a++) {
                    if ($_POST["attack" . $a] < 1 || $_POST["attack" . $a] > 3) {
                        $attacks .= 0;
                    } else {
                        $attacks .= $_POST["attack" . $a];
                    }

                }

                for ($b = 1; $b <= 6; $b++) {
                    if ($_POST["block" . $b] < 1 || $_POST["block" . $b] > 3) {
                        $blocks .= 0;
                    } else {
                        $blocks .= $_POST["block" . $b];
                    }
                }

                // create instance of match repository class
                $match = new MatchRepository($database);

                // if the match metadata couldn't be added
                if (!$match->addIndex($_SESSION['username'], $_POST["defender"])) {
                    // set the dialog message title
                    $chops->title = 'Error';

                    // set the dialog message
                    $chops->message = 'Could not add match metadata. Contact an admin';

                    // set the back url
                    $chops->backurl = 'main.php';

                    // display it
                    $chops->display('messagedialog.tpl');

                    // and terminate
                    exit;
                }

                // if the match data couldn't be added
                if (!$match->add($attacks . ":" . $blocks)) {
                    // set the dialog message title
                    $chops->title = 'Error';

                    // set the dialog message
                    $chops->message = 'Failed to add match data, contact an admin';

                    // set the back url
                    $chops->backurl = 'main.php';

                    // display it
                    $chops->display('messagedialog.tpl');

                    // and terminate
                    exit;
                }

                //if we aren't able to obtain the user info
                if (!$user->getInfo($_POST["defender"])) {
                    // set the dialog message title
                    $chops->title = 'Error';

                    // set the dialog message
                    $chops->message = 'The match has been added and the user has been notified if they have elected to';

                    // set the back url
                    $chops->backurl = 'main.php';

                    // display it
                    $chops->display('messagedialog.tpl');

                    // and terminate
                    exit;
                } elseif ($user->getNotificationPreference()) {
                    if (!$user->email("Match Challenge From " . $_SESSION['username'],
                        "User " . $_SESSION['user'] . "has challenged you.")
                    ) {
                        // set the dialog message title
                        $chops->title = 'Error';

                        // set the dialog message
                        $chops->message = 'Failed to send mail, contact an admin.';

                        // set the back url
                        $chops->backurl = 'main.php';

                        // display it
                        $chops->display('messagedialog.tpl');

                        // and terminate
                        exit;
                    }
                } else {
                    // set the dialog message title
                    $chops->title = 'Info';

                    // set the dialog message
                    $chops->message = 'The match has been added and the user has been notified if they have elected to';

                    // set the back url
                    $chops->backurl = 'main.php';

                    // display it
                    $chops->display('messagedialog.tpl');

                    // and terminate
                    exit;
                }
            }
        }
    }
}

?>