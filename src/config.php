<?php



// mysql server host
define("DATABASE_SERVER", "localhost");

// mysql server port
define("DATABASE_SERVER_PORT", "3306");

// mysql server username
define("DATABASE_USERNAME", "root");

// mysql server password
define("DATABASE_PASSWORD", "");

// mysql database name
define("DATABASE_NAME", "ae_dev");

// mysql database table prefix
define("TABLE_PREFIX", "ae_");

/* contact info */

// email address used for outgoing emails (match relay, lost passwords etc.)
define("ADMIN_EMAIL", "admin@domain.com");

/* Security Optionons */

// if true each account requires a unique email addres otherwise, multiple accounts can be created with a single email address
define("REQUIRE_UNIQUE_EMAIL", true);

// if true each account requires a unique ip addres otherwise, multiple accounts can be created with a single ip address
define("REQUIRE_UNIQUE_IP", true);

// max amount of inactivity time before the scession times out
define("SESSION_MAX_INACTIVITY_LIFETIME", 1);

/* Ranks */
define("RANK1_NAME", "White Belt");
define("RANK1_XP", 0);

define("RANK2_NAME", "Yellow Belt");
define("RANK2_XP", 2001);

define("RANK3_NAME", "Orange Belt");
define("RANK3_XP", 5001);

define("RANK4_NAME", "Green Belt");
define("RANK4_XP", 25001);

define("RANK5_NAME", "Blue Belt");
define("RANK5_XP", 50001);

define("RANK6_NAME", "Purple Belt");
define("RANK6_XP", 125001);

define("RANK7_NAME", "Brown Belt");
define("RANK7_XP", 300001);

define("RANK8_NAME", "Red Belt");
define("RANK8_XP", 500001);

define("RANK9_NAME", "Black Belt");
define("RANK9_XP", 800001);

/* Table Names */

// users table
define("TABLE_USERS", TABLE_PREFIX . "users");

// matches index table
define("TABLE_MATCHESINDEX", TABLE_PREFIX . "matchesindex");

// matches table
define("TABLE_MATCHES", TABLE_PREFIX . "matches");

/* Advanced Settings */

// users table

define("USER_CONTENT_TYPE", "text/html;charset=UTF-8");

?>