<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {

    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} // otherwise show the user's matches
else {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    // create instance of database class
    $database = new Database();

    // create instance of match class
    $matches = new MatchRepository($database);

    // create instance of chops library
    $chops = new Chops();

    // if the user has at least 1 match
    if ($matches->getCount($_SESSION['username'], $matches::MATCH_TYPE_ALL, $matches::MATCH_STATUS_OPEN) == 0) {
        // set the dialog title
        $chops->title = 'Info';

        // set the dialog message stating there are no pending matches
        $chops->message = 'There are no pending matches';

        // set the back url
        $chops->backurl = 'main.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } else {
        // get the total number of home matches
        $totalhomematches = $matches->getCount($_SESSION['username'], $matches::MATCH_TYPE_HOME,
            $matches::MATCH_STATUS_OPEN);

        // get the total number of away matches
        $totalawaymatches = $matches->getCount($_SESSION['username'], $matches::MATCH_TYPE_AWAY,
            $matches::MATCH_STATUS_OPEN);

        // if the user has both challenged others and has been challenged
        if ($totalhomematches > 0 && $totalawaymatches > 0) {
            // get the home matches
            if (!$chops->homeMatchData = $matches->getMatches($_SESSION['username'], $matches::MATCH_TYPE_HOME,
                $matches::MATCH_STATUS_OPEN, 5)
            ) {

            }

            // get the away matches
            if (!$chops->visitorMatchData = $matches->getMatches($_SESSION['username'], $matches::MATCH_TYPE_AWAY,
                $matches::MATCH_STATUS_OPEN, 5)
            ) {

            }

            // display it
            $chops->display('viewchallenges.tpl');

            // and terminate
            exit;
        } // no one has challenged the player yet
        elseif ($totalhomematches > 0 && $totalawaymatches == 0) {
            // get only the matches the player has initiated
            if (!$chops->homeMatchData = $matches->getMatches($_SESSION['username'], $matches::MATCH_TYPE_HOME,
                $matches::MATCH_STATUS_OPEN, 5)
            ) {

            }

            // display it
            $chops->display('viewchallenges_challengeronly.tpl');

            // and terminate
            exit;
        } // otherwise the user only has matches from other players
        else {
            // get only the away matches
            if (!$chops->visitorMatchData = $matches->getMatches($_SESSION['username'], $matches::MATCH_TYPE_AWAY,
                $matches::MATCH_STATUS_OPEN, 5)
            ) {

            }

            // display it
            $chops->display('viewchallenges_defenderonly.tpl');

            // and terminate
            exit;
        }
    }
}

?>