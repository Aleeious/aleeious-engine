<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Profile</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1><?php echo $username; ?>'s Profile!</h1>

<h2>Details</h2>
<ul>
    <li>Your Username: <?php echo $username; ?></li>
    <li>Your Password: ****** <a href="changepassword.php" title="Click here to change your password" accesskey="1">Change
            Password</a></li>
    <li>Your Email: <?php echo $email; ?></li>
    <li>Your Notification Preference: <?php echo $notificationpreference; ?> <a href="changenotificationpreferences.php"
                                                                                title="Click here to change your Notification Preferences"
                                                                                accesskey="1">Change Notification
            Preferences</a></li>
    <li>XP: <?php echo $xp; ?></li>
    <li>Rank: <?php echo $rank; ?></li>
</ul>
<p><a href="main.php" title="Click here to return to the main menu" id="nav" accesskey="2">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="3">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>