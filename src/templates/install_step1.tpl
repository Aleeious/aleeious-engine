<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Checking Environment</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Checking Environment</h1>
<table>
    <tr>
        <th>Item</th>
        <th>Requirement</th>
        <th>Installation</th>
    </tr>
    <tr>
        <th scope="row">PHP Version</th>
        <td>5.3</td>
        <td><?php echo $phpversion; ?></td>
        <td><?php echo $phpversionstatus; ?></td>
    </tr>
    <tr>
        <th scope="row">MySQLi Extension</th>
        <td>Any</td>
        <td><?php echo $mysqliversion; ?></td>
        <td><?php echo $mysqliversionstatus; ?></td>
    </tr>
</table>
<form action="" method="post">
    <fieldset>
        <input type="submit" name="submit" id="submit"
               title="click here to continue the installation" <?php echo $disabled; ?> accesskey="1" value="continue"
        />
    </fieldset>
</form>
</body>
</html>