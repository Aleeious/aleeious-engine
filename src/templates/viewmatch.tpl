<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - View A Match</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Your Matches</h1>
<table>
    <tr>
        <th scope="col">Challenger</th>
        <th scope="col">Defender</th>
        <th scope="col">View</th>
    </tr>
</table>
<h1>View Someone else's match</h1>

<form action="" method="get">
    <fieldset>
        <label for="id">Match ID:</label><input type="text" name="id" id="id" inputmode="" accesskey="1"/>
        <input type="submit" name="submit" id="submit" value="View!" title="click here to view the match"
               accesskey="2"/>
    </fieldset>
</form>
<p><a href="main.php" title="Click here to return to the main menu" id="nav" accesskey="1">back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="2">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>