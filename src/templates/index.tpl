<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="AleeiousMMO"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-title" content="AleeiousMMO"/>
</head>
<body>
<h1>Welcome</h1>

<p>Welcome Martial Artist. Do you think you are the best of the best? Well why not find out!</p>

<form action="" method="post">
    <fieldset>
        <label for="username">Username</label><input type="text" name="username" id="username"
                                                     title="enter your username here" inputmode="latin" accesskey="1"/>
        <label for="password">Password</label><input type="password" name="password" id="password"
                                                     title="enter your password here" inputmode="latin" accesskey="2"/>
        <input type="submit" name="submit" id="submit" value="Login!" title="click here to start playing"
               accesskey="3"/>
    </fieldset>
</form>
<p>
    <a href="register.php" title="click to register for a new account" accesskey="4">[register]</a>
    <a href="forgotusername.php" title="click to retrieve your username" accesskey="5">[forgot username]</a>
    <a href="forgotpassword.php" title="click to reset your password" accesskey="6">[forgot password]</a>
    <a href="privacypolicy.php" title="click to read the privacy policy" accesskey="7">[privacy policy]</a>
    <a href="tos.php" title="click to read the terms of service" accesskey="8">[Terms of Service]</a>
</p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="9">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>