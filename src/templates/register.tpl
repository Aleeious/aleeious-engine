<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Aleeious - Register</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Register</h1>

<p>Please fill out the application below to register for an account. A temporary password will be sent to the email you
    provided during registration.</p>

<form action="" method="post">
    <fieldset>
        <label for="antibot">Please leave this field blank</label><input type="text" name="antibot" id="antibot"
                                                                         title="please leave this field blank, it is a means to prevent bot from registering"
                                                                         inputmode="latin" accesskey="1"/>
        <label for="username">Username</label><input type="text" name="username" id="username"
                                                     title="enter your user name id here" inputmode="latin"
                                                     accesskey="2"/>
        <label for="email">Email</label><input type="text" name="email" id="email" title="enter your email address here"
                                               inputmode="latin" accesskey="3"/>

        <p>By registering you agree to our <a href="tos.php" title="Terms of Service" accesskey="4">Terms of Service</a>
            and <a href="privacypolicy.php" title="Privay Policy" accesskey="5">Privacy Policy</a></p>
        <input type="submit" name="submit" id="submit" value="Sumbit Application!"
               title="click here to complete registration" accesskey="6"/>
    </fieldset>
</form>
<p><a href="index.php" title="Click here to return to the login page" id="nav" accesskey="8">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="9">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>