<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Installing</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta http-equiv=refresh content="10; url=index.php">
    .
</head>
<body>
<h1>Installing</h1>
<table>
    <th>Database Table</th>
    <th>Status</th>
    <tr>
        <th scope="row">Users</th>
        <td><?php echo $usertablestatus; ?></td>
    </tr>
    <tr>
        <th scope="row">Match Index</th>
        <td><?php echo $matchindextablestatus; ?></td>
    </tr>
    <tr>
        <th scope="row">Match</th>
        <td><?php echo $matchtablestatus; ?></td>
    </tr>
</table>
<p>Please make sure to delete this installer, to prevent unintended installations</p>
</body>
</html>