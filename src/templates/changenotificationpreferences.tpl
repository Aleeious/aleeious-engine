<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Change Notification Preferences</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Notification Preferences</h1>

<p>This will enable notifications whenever a match is stated, cancelled or completed. Choose yes to enable notifications
    or no to disable them.</p>

<form action="" method="post">
    <fieldset>
        <legend>Preference</legend>
        <select name="preference" accesskey="1">
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
        <input type="submit" name="submit" id="submit" title="click here to change your preferences" accesskey="2"
               value="change"/>
    </fieldset>
</form>
<p><a href="profile.php" title="Click here to return to the profile page" id="nav" accesskey="3">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="4">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>