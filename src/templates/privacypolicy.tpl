<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Privacy Policy</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Privacy Policy</h1>

<p>We will not, under any circumstances whatsoever, give out or sell your information (including email address) to
    anyone.</p>

<p>We will also never send you an email unless it is important and directly related to your website experience(lost
    username, password reset etc.).</p>

<p>Also, no one will ever have access to your personal information and other data. The only exceptions to this rule:</p>

<p>From time to time, we will perform system updates, routine maintenance, and patch bugs and/or exploits. However, all
    your data will still remain 100%
    confidential. It's possible that we will be required by subpoena or other legal action to grant access to an
    individual in the process of an investigation.</p>

<p><a href="index.php" title="Click here to return to the login page" id="nav" accesskey="1">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="2">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>