<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Initiate Challange</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Start Challenge!</h1>

<form action="" method="post">
    <label for="defender">Defender</label><input type="text" name="defender" id="defender"
                                                 title="type the username of the person you want to challenge"
                                                 inputmode="latin" tabindex="1">
    <fieldset>
        <legend>Attacks</legend>
        <select name="attack1" tabindex="2">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
        <select name="attack2" tabindex="3">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
        <select name="attack3" tabindex="4">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
        <select name="attack4" tabindex="5">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
        <select name="attack5" tabindex="6">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
        <select name="attack6" tabindex="7">
            <option value="3">Attack High</option>
            <option value="2">Attack Medium</option>
            <option value="1">Attack Low</option>
        </select>
    </fieldset>
    <fieldset>
        <legend>Blocks</legend>
        <select name="block1" tabindex="8">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
        <select name="block2" tabindex="9">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
        <select name="block3" tabindex="10">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
        <select name="block4" tabindex="11">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
        <select name="block5" tabindex="12">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
        <select name="block6" tabindex="13">
            <option value="3">Block High</option>
            <option value="2">Block Medium</option>
            <option value="1">Block Low</option>
        </select>
    </fieldset>
    <input type="submit" name="submit" id="submit" title="click here to send challenge" tabindex="14" value="challenge">
</form>
<p><a href="main.php" title="Click here to return to the main menu" id="nav" accesskey="1">back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="2">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>