<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Aleeious - Change Password</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Change Password</h1>

<p>Please enter your current password and new password twice then click submit.</p>

<form action="" method="post">
    <fieldset>
        <label for="password">Password</label><input type="password" name="password" id="password"
                                                     title="enter your current passwordhere" inputmode="latin"
                                                     accesskey="1"/>
        <label for="newpassword">New Password</label><input type="password" name="newpassword" id="newpassword"
                                                            title="enter your new password here" inputmode="latin"
                                                            accesskey="2"/>
        <label for="verifypassword">Verify Password</label><input type="password" name="verifypassword"
                                                                  id="verifypassword"
                                                                  title="reenter your new password here"
                                                                  inputmode="latin" accesskey="3"/>
        <input type="submit" name="submit" id="submit" value="Sumbit Application!"
               title="click here to change your password" accesskey="4"/>
    </fieldset>
</form>
<p><a href="profile.php" title="Click here to return to the profile page" id="nav" accesskey="5">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="6">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>