<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - <?php echo $title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1><?php echo $title; ?></h1>

<p><?php echo $message; ?>.</p>

<p><a href="<?php echo $backurl; ?>" title="click here to return to the previous page" id="nav" accesskey="1">back</a>
</p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="2">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>