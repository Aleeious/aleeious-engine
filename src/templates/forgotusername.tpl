<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Aleeious - Forgot Username</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
</head>
<body>
<h1>Forgot Your Username?</h1>

<p>Please fill out the application below to send a reminder of you username.</p>

<form action="" method="post">
    <fieldset>
        <label for="email">Email</label><input type="text" name="email" id="email" title="enter your email address here"
                                               inputmode="latin" accesskey="1"/>
        <input type="submit" name="submit" id="submit" value="Sumbit Application!"
               title="click here to have your username sent" accesskey="2"/>
    </fieldset>
</form>
<p><a href="index.php" title="Click here to return to the login page" id="nav" accesskey="3">Back</a></p>

<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="4">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>