<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Aleeious - Welcome</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="templates/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="templates/css/print.css" media="print"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="AleeiousMMO"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-title" content="AleeiousMMO"/>
</head>
<body>
<h1>Welcome <?php echo $username; ?></h1>
<ul>
    <li><a href="challenge.php" title="click to challenge someone" accesskey="1">Challenge Someone</a></li>
    <li><a href="viewchallenges.php" title="click to view your pending challenges" accesskey="2">View Challenges</a>
    </li>
    <li><a href="profile.php" title="click to view your profile" accesskey="3">Profile</a></li>
    <li><a href="logout.php" title="click to log out" accesskey="4">Log Out</a></li>
</ul>
<div id="footer"><p><a href="http://www.aleeious.tk" title="Powered By The Aleeious Engine" accesskey="5">Powered By The
            Aleeious Engine</a></p></div>
</body>
</html>