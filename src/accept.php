<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start session
session_start();

// if the user isn't logged in
if (!isset($_SESSION['username'], $_SESSION['lastactivity'])) {
    // redirect to the login form
    header("Location: index.php");

    // and terminate
    exit;
} // if the user has been inactive
elseif (time() - $_SESSION['lastactivity'] > SESSION_MAX_INACTIVITY_LIFETIME * 60) {

    // unset all session variables
    session_unset();

    // distroy the session
    session_destroy();

    // redirect to the login form
    header("Location: logout.php");
} // otherwise the user is logged in and active
else {
    // update the session timeout timer
    $_SESSION['lastactivity'] = time();

    // create instance of chops library
    $chops = new chops();

    // if no id was provided
    if (empty($_GET["id"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating there is tampering detected
        $chops->message = 'Tampering detected, don\'t do that';

        // set the back link
        $chops->backurl = 'index.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the id isn't numeric
    elseif (!is_numeric($_GET["id"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the username is empty
        $chops->message = 'Tampering detected, don\'t do that';

        // set the back link
        $chops->backurl = 'index.php';

        // and display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } else {
        $_GET["id"] = trim(strip_tags($_GET["id"]));
    }

    // create instance of database class
    $database = new Database();

    // create instance of match repository class
    $match = new MatchRepository($database);

    // if the match data was retrieved
    if (!$matchData = $match->getMatch($_GET["id"])) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating there was an error
        $chops->message = 'Failed to contact the database, notify an admin.';

        // set the back url
        $chops->backurl = 'main.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    } // if the user didn't recieve the match request
    elseif ($matchData[0]->getDefender() != $_SESSION["username"]) {
        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the user is tampering
        $chops->message = 'Tampering Detected, don\'t do that';

        // set the back url
        $chops->backurl = 'main.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    }


    if (!isset($_POST["submit"])) {
        // set the challenger
        $chops->challenger = $matchData[0]->getChallenger();

        // display it
        $chops->display('accept.tpl');

        // and terminate
        exit;
    } else {
        // string to hold attack values
        $attacks = "";

        // string to hold block values
        $blocks = "";

        for ($a = 1; $a <= 6; $a++) {
            if ($_POST["attack" . $a] < 1 || $_POST["attack" . $a] > 3) {
                $attacks .= 0;
            } else {
                $attacks .= $_POST["attack" . $a];
            }

        }

        for ($b = 1; $b <= 6; $b++) {
            if ($_POST["block" . $b] < 1 || $_POST["block" . $b] > 3) {
                $blocks .= 0;
            } else {
                $blocks .= $_POST["block" . $b];
            }
        }

        // create the defenders pattern using the inputted info
        $defenderpattern = $attacks . ":" . $blocks;

        echo "Defender Pattern: " . $defenderpattern;
        // create instance of user class
        $user = new User($database);

        // update the match
        if (!$match->updateMatch($_GET["id"], $defenderpattern)) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating there was an error
            $chops->message = 'Failed to update the match, notify an admin.';

            // set the back url
            $chops->backurl = 'main.php';

            // display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }

        // change the match's status to completed
        if (!$match->changeStatus($_GET["id"], $_SESSION["username"], $match::MATCH_STATUS_COMPLETED,
            $match::MATCH_TYPE_AWAY)
        ) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating there was an error
            $chops->message = 'Failed to update the match status, notify an admin.';

            // set the back url
            $chops->backurl = 'main.php';

            // display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }

        // get the other users info
        if (!$user->getInfo($matchData[0]->getChallenger())) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating there was an error
            $chops->message = 'Failed to contact the database, notify an admin.';

            // set the back url
            $chops->backurl = 'main.php';

            // display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }

        // if the user wants to be notified
        if ($user->getNotificationPreference()) {
            if (!$user->email("Match Completed",
                "User " . $_SESSION["user"] . "has completed the match and it is available for viewing.")
            ) {
                // set the dialog title
                $chops->title = 'Error';

                // set the dialog message stating the feature hasn't been implemented yet'
                $chops->message = 'Could not send notification email. Notify the admin';

                // set the back url
                $chops->backurl = 'main.php';

                // display it
                $chops->display('messagedialog.tpl');

                echo "mail() not ok!";
                // and terminate
                exit;
            }
        }

        // set the dialog title
        $chops->title = 'Error';

        // set the dialog message stating the feature hasn't been implemented yet'
        $chops->message = 'This feature hasn\'t been implemented yet';

        // set the back url
        $chops->backurl = 'main.php';

        // display it
        $chops->display('messagedialog.tpl');

        // and terminate
        exit;
    }
}

?>