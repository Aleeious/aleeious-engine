<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of Chops library
$chops = new Chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// start the session
session_start();

// if the user is already logged in
if (isset($_SESSION['username'], $_SESSION['lastactivity'])) {
    // redirect to the game
    header("Location: main.php");

    // and terminate
    exit;
} // if the form wasn't submited
else {
    if (!isset($_POST["submit"])) {
        // display it
        $chops->display('index.tpl');

        // and terminate
        exit;
    } // otherwise the form was submitted
    else {
        // if the username is empty
        if (empty($_POST["username"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the username is empty
            $chops->message = 'username is empty';

            // set the back link
            $chops->backurl = 'index.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // if the username is too short or too big
        elseif (strlen($_POST["username"]) < 4 || strlen($_POST["username"]) > 16) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the username is too short or too big
            $chops->message = 'username must be 4-16 characters long';

            // set the back link
            $chops->backurl = 'index.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } elseif (!preg_match("#^[a-z0-9]*$#", $_POST["username"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the username contains invalid characters
            $chops->message = 'username contains invalid characters';

            // set the back url
            $chops->backurl = 'index.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // otherwise the username is filled in
        else {
            // so sanitize it
            $_POST["username"] = trim(strip_tags($_POST["username"]));
        }

        // if the password is empty
        if (empty($_POST["password"])) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the password is empty
            $chops->message = 'password is empty';

            // set the back url
            $chops->backurl = 'index.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // if the password is too short or too big
        elseif (strlen($_POST["password"]) < 6 || strlen($_POST["password"]) > 32) {
            // set the dialog title
            $chops->title = 'Error';

            // set the dialog message stating the password is too short or too big
            $chops->message = 'password must be 6-32 characters long';

            // set the back url
            $chops->backurl = 'index.php';

            // and display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        } // otherwise the password is ok
        else {
            // so sanitize it
            $_POST["password"] = trim(strip_tags($_POST["password"]));
        }

        // create instance of database class
        $database = new Database();

        // create instance of user class
        $user = new User($database);

        // check the users login info
        if ($user->checkLogin($_POST["username"], $_POST["password"])) {
            // store info in session data
            $_SESSION['username'] = $user->getUsername();
            $_SESSION['lastactivity'] = time();

            // sanatize user info
            $user->sanitize();

            // redirect to the game
            header("Location: main.php");
        } else {
            // set the dialog title
            $chops->title = 'Error';

            // otherwise set the message dialog stating the login is incorrect
            $chops->message = 'login is not correct';

            // set the back url
            $chops->backurl = 'index.php';

            // display it
            $chops->display('messagedialog.tpl');

            // and terminate
            exit;
        }
    }
}

?>