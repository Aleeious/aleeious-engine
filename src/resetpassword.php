<?php

/*
 *   This file is part of Aleeious.
 *
 *   Aleeious is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Aleeious is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Aleeious.  If not, see <http://www.gnu.org/licenses/>.
 */

// disable display or error messages and log them instead
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/logs/error_log');

// include autoloader
require('libs/autoload.php');

// include configuration data
require_once('config.php');

// create instance of smarty library
$chops = new chops();

// set content header
header("Content-Type: " . USER_CONTENT_TYPE);

// if the username is empty
if (!isset($_GET["username"])) {
    // set the dialog title
    $chops->title = 'Error';

    // set the dialog message stating the username is empty
    $chops->message = 'username is empty';

    // set the back url
    $chops->backurl = 'forgotpassword.php';

    // and display it
    $chops->display('messagedialog.tpl');

    // and terminate
    exit;
} // if the username is too short or too big
elseif (strlen($_GET["username"]) < 4 || strlen($_GET["username"]) > 16) {
    // set the dialog title
    $chops->title = 'Error';

    // set the dialog message stating the username is empty
    $chops->message = 'username must be 4-16 characters long';

    // set the back url
    $chops->backurl = 'forgotpassword.php';

    // and display it
    $chops->display('messagedialog.tpl');

    // and terminate
    exit;
} elseif (!preg_match("#^[a-z0-9]*$#", $_GET["username"])) {
    // set the dialog title
    $chops->title = 'Error';

    // set the dialog message stating the username contains invalid characters
    $chops->message = 'username contains invalid characters';

    // set the back url
    $chops->backurl = 'forgotpassword.php';

    // and display it
    $chops->display('messagedialog.tpl');

    // and terminate
    exit;
} // otherwise the username is filled in
else {
    // so sanitize it
    $username = $_GET["username"];
}

// if the email is empty
if (!isset($_GET["key"])) {
    // set the message dialog
    $chops->title = 'Error';

    // set the dialog message stating the unique key is empty
    $chops->message = 'unique key is empty';

    // set the back url
    $chops->backurl = 'forgotpassword.php';

    // and display it
    $chops->display('messagedialog.tpl');

    // and terminate
    exit;
} else {
    // so sanitize it
    $uniqueid = $_GET["key"];
}

// create instance of database class
$database = new Database();

// create instance of user class
$user = new User($database);

// check the users login info
if (!$user->checkPasswordChangeRequest($username, $uniqueid)) {
    // set the dialog title
    $chops->title = 'Error';

    // otherwise display an error stating the database couldn't be contacted
    $chops->message = 'the password change request is invalid';

    // set the back url to the main page
    $chops->backurl = 'index.php';

    // display it
    $chops->display('messagedialog.tpl');
} else {
    if (!$user->resetPassword($username, $user->getEmail())) {
        // set the dialog title
        $chops->title = 'Info';

        // otherwise set the dialog message stating the password will be sent
        $chops->message = "The password couldn't be changed because an error occured. Please contact the admin";

        // set the back url
        $chops->backurl = 'index.php';

        // display it
        $chops->display('messagedialog.tpl');
    } else {
        // email the user the new password
        if (!$user->email("Your New Password",
            "Someone possibly you has asked to have your password reset. Your new password is: " . $user->getPassword() . ".")
        ) {
            // set the dialog title
            $chops->title = 'Error';

            // otherwise set the dialog message stating the password will be sent
            $chops->message = 'Could not send new password. Please contact the admin.';

            // set the back url
            $chops->backurl = 'index.php';

            // display it
            $chops->display('messagedialog.tpl');
        } else {
            if ($user->clearChangeRequest($username)) {
                // set the dialog title
                $chops->title = 'Info';

                // otherwise set the dialog message stating the password will be sent
                $chops->message = 'A new password will be generated and sent to your email';

                // set the back url
                $chops->backurl = 'index.php';

                // display it
                $chops->display('messagedialog.tpl');
            }
        }
    }

    // sanatize everything
    $user->sanitize();
}

?>